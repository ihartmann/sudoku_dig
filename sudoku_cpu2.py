MATRIX_SIZE = 9
SQRT_MATRIX_SIZE = 3
SUMA = 45


def analyze_rows (lista, a, p, i):
    for j in range (i - p, i - p + MATRIX_SIZE):
        if a[j] not in lista:
            lista.append(a[j])
    return lista

def analyze_columns (lista, a, p, i):
    m = range (p, p+ (MATRIX_SIZE - 1) * MATRIX_SIZE + 1)
    m = m[::MATRIX_SIZE]
    for j in m:
        if a[j] not in lista:
            lista.append(a[j])
    return lista


def analyze_squares (lista, a, p, i, q):
    n = []
    for l in range (0, MATRIX_SIZE):
        n.append(i - ((p % SQRT_MATRIX_SIZE) + (q % SQRT_MATRIX_SIZE) * MATRIX_SIZE) + (l / SQRT_MATRIX_SIZE) * MATRIX_SIZE + (l % SQRT_MATRIX_SIZE))
      
    for j in range (0, MATRIX_SIZE):
        if a[n[j]] not in lista:
            lista.append(a[n[j]])
    return lista

def sudoku (a):
    kraj = 0
    while kraj!=45*9:
        for i in range (0,81):
            lista = []
            if a[i]==0:
                p = i % MATRIX_SIZE
                q = (i-p)/MATRIX_SIZE
                lista = analyze_columns (lista, a, p, i)
                lista = analyze_squares (lista, a, p, i, q)
                lista = analyze_rows (lista, a, p, i)
                if len(lista)==9:
                    a[i] = SUMA - sum(lista)                    
        kraj = sum(a)

a = [6,0,0,0,0,0,0,4,0,0,9,0,2,3,5,1,0,0,3,0,0,6,0,0,9,0,0,0,0,9,4,1,0,7,0,2,7,4,1,0,0,0,6,3,9,2,0,6,0,7,9,8,0,0,0,0,8,0,0,4,0,0,1,0,0,4,1,2,3,0,6,0,0,2,0,0,0,0,0,0,7]

sudoku(a)
print a

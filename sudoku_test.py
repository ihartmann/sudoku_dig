#!/usr/bin/env python
# -*- coding: utf-8 -*-

#     """
#     Pokreće CPU i GPU programe za rješavanje sudokua i prikazuje
#     vrijeme koje je bilo potrebno da se izvrše
# 
#     Vraća:
#     Podatke o vremenu potrebnom da se kodovi izvrše
#
#     """

import time
import sudoku_solver

t0 = time.time()
execfile('sudoku_cpu.py')
t1 = time.time()
execfile('sudoku_cpu2.py')
t2 = time.time()
execfile('sudoku_solver.py')
t3 = time.time()
print 'CPU %f' %(t1-t0)
print 'CPU (alter) %f' %(t2-t1)
print 'GPU %f' %(t3-t2)


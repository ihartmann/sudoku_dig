import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("sudoku_solver.cu").read())

sudoku2 = mod.get_function("sudoku2")
sudoku_check_solution = mod.get_function("sudoku_check_solution")

a = np.array([[6,0,0,0,0,0,0,4,0],
              [0,9,0,2,3,5,1,0,0],
              [3,0,0,6,0,0,9,0,0],
              [0,0,9,4,1,0,7,0,2],
              [7,4,1,0,0,0,6,3,9],
              [2,0,6,0,7,9,8,0,0],
              [0,0,8,0,0,4,0,0,1],
              [0,0,4,1,2,3,0,6,0],
              [0,2,0,0,0,0,0,0,7]],dtype=np.int32)

print a

result_gpu1 = np.zeros_like(a)


sudoku2(drv.Out(result_gpu1), drv.In(a),
       block=(81,1,1), grid=(1,1))



jel_ok = np.zeros((3, 9), dtype=np.bool);
sudoku_check_solution(drv.Out(jel_ok), drv.In(result_gpu1),
                      block=(9, 1, 1), grid=(3, 1))

print "GPU rezultat \n", result_gpu1
print jel_ok


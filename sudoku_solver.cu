#include<stdio.h>
const int MATRIX_SIZE = 9;
const int SQRT_MATRIX_SIZE = 3;
const int SUMA = 45;


__device__ void nullify_list (int *lista)
{
  for (int j = 0; j< MATRIX_SIZE + 1; j++)
    {
      lista[j]=0;
    } //inicijalizacija liste u koju cu stavljati znamenke
}


__device__ void analyze_rows (int *lista, int *a, int p, int i)
{
  for (int j = i - p; j < i - p + MATRIX_SIZE; j++) //prolaz po svim elementima reda
    {
      
      if (lista[a[j]]==0)//trpamo u listu sve pronadjene brojeve u redu, kasnije u stupcu i pravokutniku
	{
	  lista[a[j]]=a[j];
	}    
    }
  }


__device__ void analyze_columns (int *lista, int *a, int p, int i)
{
  for (int j = p; j <= p+ (MATRIX_SIZE - 1) * MATRIX_SIZE; j = j+ MATRIX_SIZE) //trpanje u listu i svih brojeva pronadjenih u nekom stupcu
    {
      
      if (lista[a[j]]==0)
	{
	  lista[a[j]]=a[j];
	}
    }
}


__device__ void	analyze_squares (int *lista, int *a, int p, int i, int q)
{
  int n[MATRIX_SIZE];
  for (int l = 0; l < MATRIX_SIZE; l++)
    {
     
      // veliki kvadrat vertikalno + veliki kvadrat horizontalno + mali kvadrat vertikalno + mali kvadrat horizontalno
      // za pomak uzet u obzir redak i stupac elementa
      n[l] = i - ((p % SQRT_MATRIX_SIZE) + (q % SQRT_MATRIX_SIZE) * MATRIX_SIZE) + (l / SQRT_MATRIX_SIZE) * MATRIX_SIZE + (l % SQRT_MATRIX_SIZE);
    }
      
  for (int j=0; j < MATRIX_SIZE; j++)
    {
      if (lista[a[n[j]]] == 0)
	{
	  lista[a[n[j]]] = a[n[j]];
	}
    }
}

__global__ void sudoku2 (int *dest, int *a)
{
  //ideja je da sve brojeve iz stupca i retka (i kasnije iz pripadajuceg dijela grida) utrpam u jedan niz i onda pronadjem koji broj nedostaje
  
  const int i = threadIdx.x;
  const int p = i % MATRIX_SIZE; //polozaj u redu
  const int q = (i - p) / MATRIX_SIZE; // polozaj u stupcu 
  //printf ("\nthread %d p %d q %d",i,p,q);
  int failsafe = 0;
  
  while (a[i] == 0)
  {
    failsafe++;
    
      int lista[MATRIX_SIZE + 1]; //lista u koju stavljam znamenke iz redaka, stupaca i kvadrata
      nullify_list (lista);
      
      analyze_rows (lista, a, p, i);
      analyze_columns (lista, a, p, i);
      analyze_squares (lista, a, p, i, q);
      //provjera liste
      /*
      for (int j = 0; j < MATRIX_SIZE + 1; j++)
	{
	  printf ("Nit %d element liste %d ima vrijednost %d\n", i, j, lista[j]);
	}
      */
  
      
      //brojac nula, morale bi biti tocno dvije da bi bilo jedinstveno rjesenje
      
      int brojac = 0;
      for (int j = 0; j < MATRIX_SIZE + 1; j++)
	{
	  if (lista[j] == 0)
	    {
	      brojac++;
	    }
	}
     
      //izracun rjesenja
      if (brojac == 2)
	{
	  for (int j = 0; j <= MATRIX_SIZE; j++)
	    {
	      a[i] = a[i] + lista[j];
	    }
	  a[i] = SUMA - a[i];
	  //printf ("\n thread %d ... rjesenje... %d",i, a[i]);
	}

       __syncthreads();
       if (failsafe>81) {a[i]=1;}
  }
  dest[i] = a[i];
 
}


__device__ bool provjeri_listu (int* red)
{
  int suma = 0;
  for (int i = 0; i < MATRIX_SIZE; i++)
    {
      suma += red[i];
    }
  if (suma == SUMA)
    {
      return true;
    }
  else
    {
      return false;
    }
}


__global__ void sudoku_check_solution (bool* ok, int *a)
{
  const int i = blockIdx.x * blockDim.x + threadIdx.x;
  const int tI = threadIdx.x;
  const int bI = blockIdx.x;
  
  if (bI == 0)
    {
      int redak[MATRIX_SIZE];
      for (int j = 0; j < MATRIX_SIZE; j++)
	{
	  redak[j] = a[j + MATRIX_SIZE * tI];
	  //printf("Nit %d indeks retka %d\n", i, j + MATRIX_SIZE * tI);
	}
      ok[i] = provjeri_listu (redak);
     
    }
  else if (bI == 1)
    {
      int stupac[MATRIX_SIZE];
      for (int j = 0; j < MATRIX_SIZE; j++)
	{
	  stupac[j] = a[tI + j * MATRIX_SIZE];
	  //printf("Nit %d indeks stupca %d\n", i, tI + j * MATRIX_SIZE);
	}
      ok[i] = provjeri_listu (stupac);
      
    }
  else if (bI == 2)
    {
      int kvadrat[MATRIX_SIZE];
      for (int j = 0; j < MATRIX_SIZE; j++)
	{

	  kvadrat[j] = a[tI - (((tI % MATRIX_SIZE) % SQRT_MATRIX_SIZE) + (((tI - (tI % MATRIX_SIZE)) / MATRIX_SIZE) % SQRT_MATRIX_SIZE) * MATRIX_SIZE) + (j/SQRT_MATRIX_SIZE) * MATRIX_SIZE + j%SQRT_MATRIX_SIZE];
	  
	  
	}
      ok[i] = provjeri_listu (kvadrat);
      
    }
}
